FROM golang:1.21-alpine AS builder

WORKDIR /crypto-api

COPY go.mod go.sum ./

RUN apk --no-cache add bash make

RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -o ./cmd/app/crypto ./cmd/app/main.go

FROM alpine AS runner

WORKDIR /crypto-api

COPY --from=builder crypto-api/cmd/app/crypto .
COPY --from=builder crypto-api/config/config.yaml ./config/ 
COPY --from=builder crypto-api/.env . 

CMD ["./crypto"]

EXPOSE 24680