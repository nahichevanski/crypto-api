package cryptocompare

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
	"time"
)

// https://min-api.cryptocompare.com/data/pricemulti?fsyms=BTC,ETH&tsyms=USD&api_key={your_api_key}

const (
	USD = "USD"
)

var (
	apiKey = "api_key=" + os.Getenv("API_KEY")
	url    = "https://min-api.cryptocompare.com/data/pricemulti?fsyms="
	client = http.Client{Timeout: 5 * time.Second}
)

func RateFromCryptocompare(coinName ...string) (string, error) {
	const pathErr = "pkg.cryptocompare.BytesFromCryptocompare"

	BTCandETH := strings.Join(coinName, ",")

	resp, err := client.Get(url + BTCandETH + "&tsyms=" + USD + "&" + apiKey)
	if err != nil {
		return "", fmt.Errorf("%s: %w", pathErr, err)
	}

	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("%s: %w", pathErr, err)
	}
	return string(body), nil
}
