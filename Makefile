run: ## Start application
run:
	sudo docker-compose up

build: ## Build application
build:
	sudo docker-compose build

test: ## Start tests 
test:
	go test -cover -v ./internal/cases 



DOCKER_COMPOSE_FILE ?= docker-compose.yaml
LINKS_TABLE ?= rates


migrate-up: ## Run migrations UP
migrate-up:
	sudo docker compose -f ${DOCKER_COMPOSE_FILE} --profile tools run --rm migrate up

migrate-down: ## Rollback migrations against non test DB
migrate-down:
	sudo docker compose -f ${DOCKER_COMPOSE_FILE} --profile tools run --rm migrate down

migrate-create: ## Create a DB migration files e.g `make migrate-create name=migration-name`
migrate-create:
	sudo docker compose -f ${DOCKER_COMPOSE_FILE} --profile tools run --rm migrate create -ext sql -dir /migrations $(LINKS_TABLE)

shell-db: ## Enter to database console
shell-db:
	sudo docker compose -f ${DOCKER_COMPOSE_FILE} exec crypto-db psql -U puser -d dbp
