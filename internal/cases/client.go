package cases

import "crypto-api/internal/entities"

//go:generate mockgen -source=./client.go -destination=./testdata/client.go --package=testdata
type Client interface {
	GetRate(coinNames ...string) ([]entities.Coin, error)
}
