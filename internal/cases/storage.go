package cases

import (
	"context"
	"crypto-api/internal/entities"
)

//go:generate mockgen -source=./storage.go -destination=./testdata/storage.go --package=testdata
type Storage interface {
	Save(ctx context.Context, coins []entities.Coin) error
	MinDaily(ctx context.Context, name string) (entities.Coin, error)
	MaxDaily(ctx context.Context, name string) (entities.Coin, error)
	AvgDaily(ctx context.Context, name string) (entities.Coin, error)
}
