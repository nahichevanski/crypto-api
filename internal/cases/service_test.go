package cases_test

import (
	"context"
	"crypto-api/internal/cases"
	"crypto-api/internal/cases/testdata"
	"crypto-api/internal/entities"
	"log/slog"
	"os"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
)

func Test_GetCrypto_Success(t *testing.T) {

	t.Parallel()
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	storage := testdata.NewMockStorage(ctrl)
	client := testdata.NewMockClient(ctrl)
	log := slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}))

	srv, err := cases.NewService(storage, client, log)
	require.Nil(t, err)
	require.NotNil(t, srv)

	coinNames := []string{"BTC"}
	coin := entities.Coin{Name: "BTC"}
	coins := []entities.Coin{coin}

	client.EXPECT().GetRate(coinNames).Return(coins, nil)

	res, err := srv.GetCrypto(coinNames)
	require.Nil(t, err)
	require.Equal(t, coins, res)
}

func Test_SaveCrypto_Success(t *testing.T) {
	t.Parallel()
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	storage := testdata.NewMockStorage(ctrl)
	client := testdata.NewMockClient(ctrl)
	log := slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}))

	srv, err := cases.NewService(storage, client, log)
	require.Nil(t, err)
	require.NotNil(t, srv)

	coin := entities.Coin{Name: "BTC"}
	coins := []entities.Coin{coin}
	ctx := context.Background()

	storage.EXPECT().Save(ctx, coins).Return(nil)

	err = srv.SaveCrypto(ctx, coins)
	require.Nil(t, err)
}

func Test_MinDailyCrypto_Success(t *testing.T) {
	t.Parallel()
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	storage := testdata.NewMockStorage(ctrl)
	client := testdata.NewMockClient(ctrl)
	log := slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}))

	srv, err := cases.NewService(storage, client, log)
	require.Nil(t, err)
	require.NotNil(t, srv)

	ctx := context.Background()
	coinName := "BTC"
	coin := entities.Coin{Name: coinName}

	storage.EXPECT().MinDaily(ctx, coinName).Return(coin, nil)

	res, err := srv.MinDailyCrypto(ctx, coinName)
	require.Nil(t, err)
	require.Equal(t, coin, res)
}

func Test_MaxDailyCrypto_Success(t *testing.T) {
	t.Parallel()
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	storage := testdata.NewMockStorage(ctrl)
	client := testdata.NewMockClient(ctrl)
	log := slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}))

	srv, err := cases.NewService(storage, client, log)
	require.Nil(t, err)
	require.NotNil(t, srv)

	ctx := context.Background()
	coinName := "BTC"
	coin := entities.Coin{Name: coinName}

	storage.EXPECT().MaxDaily(ctx, coinName).Return(coin, nil)

	res, err := srv.MaxDailyCrypto(ctx, coinName)
	require.Nil(t, err)
	require.Equal(t, coin, res)
}

func Test_AvgDailyCrypto_Success(t *testing.T) {
	t.Parallel()
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	storage := testdata.NewMockStorage(ctrl)
	client := testdata.NewMockClient(ctrl)
	log := slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}))

	srv, err := cases.NewService(storage, client, log)
	require.Nil(t, err)
	require.NotNil(t, srv)

	ctx := context.Background()
	coinName := "BTC"
	coin := entities.Coin{Name: coinName}

	storage.EXPECT().AvgDaily(ctx, coinName).Return(coin, nil)

	res, err := srv.AvgDailyCrypto(ctx, coinName)
	require.Nil(t, err)
	require.Equal(t, coin, res)
}
