package cases

import (
	"context"
	"crypto-api/internal/entities"
	"fmt"
	"log/slog"
)

var (
	CryptoList = []string{"BTC", "ETH"}
)

type Service struct {
	storage Storage
	client  Client
	logger  *slog.Logger
}

func NewService(s Storage, c Client, logger *slog.Logger) (*Service, error) {
	return &Service{
		storage: s,
		client:  c,
		logger:  logger,
	}, nil
}

func (srv *Service) GetCrypto(coinNames []string) ([]entities.Coin, error) {
	const op = "internal.cases.GetCrypto"
	coins, err := srv.client.GetRate(coinNames...)
	if err != nil {
		srv.logger.Error("failed request in GetCrypto", err)
		return nil, fmt.Errorf("%s: %w", op, err)
	}
	srv.logger.Debug("GetCrypto was called")
	return coins, nil
}

func (srv *Service) SaveCrypto(ctx context.Context, coins []entities.Coin) error {
	const op = "internal.cases.SaveCrypto"

	err := srv.storage.Save(ctx, coins)
	if err != nil {
		srv.logger.Error("failed request in SaveCrypto", err)
		return fmt.Errorf("%s: %w", op, err)
	}
	srv.logger.Debug("SaveCrypto was called")
	return nil
}

func (srv *Service) MinDailyCrypto(ctx context.Context, name string) (entities.Coin, error) {
	const op = "internal.cases.MinDailyCrypto"

	coin, err := srv.storage.MinDaily(ctx, name)
	if err != nil {
		return entities.Coin{}, fmt.Errorf("%s: %w", op, err)
	}

	return coin, nil
}

func (srv *Service) MaxDailyCrypto(ctx context.Context, name string) (entities.Coin, error) {
	const op = "internal.cases.MaxDailyCrypto"

	coin, err := srv.storage.MaxDaily(ctx, name)
	if err != nil {
		return entities.Coin{}, fmt.Errorf("%s: %w", op, err)
	}

	return coin, nil
}

func (srv *Service) AvgDailyCrypto(ctx context.Context, name string) (entities.Coin, error) {
	const op = "internal.cases.AvgDailyCrypto"

	coin, err := srv.storage.AvgDaily(ctx, name)
	if err != nil {
		return entities.Coin{}, fmt.Errorf("%s: %w", op, err)
	}

	return coin, nil
}
