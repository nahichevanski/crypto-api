// Code generated by MockGen. DO NOT EDIT.
// Source: ./client.go

// Package testdata is a generated GoMock package.
package testdata

import (
	entities "crypto-api/internal/entities"
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
)

// MockClient is a mock of Client interface.
type MockClient struct {
	ctrl     *gomock.Controller
	recorder *MockClientMockRecorder
}

// MockClientMockRecorder is the mock recorder for MockClient.
type MockClientMockRecorder struct {
	mock *MockClient
}

// NewMockClient creates a new mock instance.
func NewMockClient(ctrl *gomock.Controller) *MockClient {
	mock := &MockClient{ctrl: ctrl}
	mock.recorder = &MockClientMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockClient) EXPECT() *MockClientMockRecorder {
	return m.recorder
}

// GetRate mocks base method.
func (m *MockClient) GetRate(coinNames ...string) ([]entities.Coin, error) {
	m.ctrl.T.Helper()
	varargs := []interface{}{}
	for _, a := range coinNames {
		varargs = append(varargs, a)
	}
	ret := m.ctrl.Call(m, "GetRate", varargs...)
	ret0, _ := ret[0].([]entities.Coin)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetRate indicates an expected call of GetRate.
func (mr *MockClientMockRecorder) GetRate(coinNames ...interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetRate", reflect.TypeOf((*MockClient)(nil).GetRate), coinNames...)
}
