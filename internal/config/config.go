package config

import (
	"log"

	"github.com/ilyakaznacheev/cleanenv"
	"github.com/joho/godotenv"
)

type Config struct {
	DB     `yaml:"db"`
	Server `yaml:"server"`
}

type DB struct {
	Host     string `yaml:"host"`
	Port     string `yaml:"port"`
	User     string `yaml:"user"`
	Name     string `yaml:"name"`
	Password string `env:"DB_PASS"`
}

type Server struct {
	Addr string `yaml:"addr"`
}

func MustLoad() *Config {

	var cfg Config

	err := cleanenv.ReadConfig("config/config.yaml", &cfg)
	if err != nil {
		log.Fatal(err)
	}

	err = godotenv.Load()
	if err != nil {
		log.Fatal(err)
	}

	return &cfg
}
