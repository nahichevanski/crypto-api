package httpserver

import (
	"crypto-api/internal/cases"
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi/v5"
)

func (srv *Server) Rates(rw http.ResponseWriter, req *http.Request) {

	ctx := req.Context()

	coins, err := srv.service.GetCrypto(cases.CryptoList)
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		rw.Write([]byte("SERVER_ERROR"))
		return
	}

	err = srv.service.SaveCrypto(ctx, coins)
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		rw.Write([]byte("SERVER_ERROR"))
		return
	}

	res, err := json.Marshal(coins)
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		rw.Write([]byte("SERVER_ERROR"))
		return
	}

	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusOK)
	rw.Write([]byte(res))
}

func (srv *Server) Minimum(rw http.ResponseWriter, req *http.Request) {

	ctx := req.Context()

	cryptocurrency := chi.URLParam(req, "cryptocurrency")

	coin, err := srv.service.MinDailyCrypto(ctx, cryptocurrency)
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		rw.Write([]byte("SERVER_ERROR"))
		return
	}

	coinBytes, err := json.Marshal(coin)
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		rw.Write([]byte("SERVER_ERROR"))
		return
	}

	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusOK)
	rw.Write(coinBytes)

}

func (srv *Server) Maximum(rw http.ResponseWriter, req *http.Request) {

	ctx := req.Context()

	cryptocurrency := chi.URLParam(req, "cryptocurrency")

	coin, err := srv.service.MaxDailyCrypto(ctx, cryptocurrency)
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		rw.Write([]byte("SERVER_ERROR"))
		return
	}

	coinBytes, err := json.Marshal(coin)
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		rw.Write([]byte("SERVER_ERROR"))
		return
	}

	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusOK)
	rw.Write(coinBytes)

}

func (srv *Server) Average(rw http.ResponseWriter, req *http.Request) {

	ctx := req.Context()

	cryptocurrency := chi.URLParam(req, "cryptocurrency")

	coin, err := srv.service.AvgDailyCrypto(ctx, cryptocurrency)
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		rw.Write([]byte("SERVER_ERROR"))
		return
	}

	coinBytes, err := json.Marshal(coin)
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		rw.Write([]byte("SERVER_ERROR"))
		return
	}

	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(http.StatusOK)
	rw.Write(coinBytes)

}
