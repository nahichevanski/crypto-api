package httpserver

import (
	"crypto-api/internal/config"
	"errors"
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

type Server struct {
	router  *chi.Mux
	service Service
}

func NewServer(s Service) (*Server, error) {
	if s == nil {
		return nil, errors.New("failed server creation attempt, server is nil")
	}
	srvr := &Server{
		router:  chi.NewRouter(),
		service: s,
	}
	return srvr, nil
}

func (srv *Server) Run(cfg *config.Config) error {
	const op = "internal.ports.server.Run"

	srv.router.Use(middleware.Logger)
	srv.router.Use(middleware.Recoverer)
	srv.router.Route("/rates", func(r chi.Router) {
		r.Get("/", srv.Rates)
		r.Get("/min/{cryptocurrency}", srv.Minimum)
		r.Get("/max/{cryptocurrency}", srv.Maximum)
		r.Get("/avg/{cryptocurrency}", srv.Average)
	})

	err := http.ListenAndServe(cfg.Server.Addr, srv.router)
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	return nil
}
