package httpserver

import (
	"context"
	"crypto-api/internal/entities"
)

type Service interface {
	GetCrypto(coinNames []string) ([]entities.Coin, error)
	SaveCrypto(ctx context.Context, coins []entities.Coin) error
	MinDailyCrypto(ctx context.Context, name string) (entities.Coin, error)
	MaxDailyCrypto(ctx context.Context, name string) (entities.Coin, error)
	AvgDailyCrypto(ctx context.Context, name string) (entities.Coin, error)
}
