package postgres

import (
	"context"
	"crypto-api/internal/config"
	"crypto-api/internal/entities"
	"fmt"
	"log/slog"
	"os"
	"time"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
)

type Storage struct {
	DB  *pgxpool.Pool
	log *slog.Logger
}

func New(ctx context.Context, log *slog.Logger, cfg *config.Config) (*Storage, error) {
	const oper = "storage.postgres.New"

	conn := fmt.Sprintf("postgres://%s:%s@%s:%s/%s",
		cfg.DB.User, os.Getenv("DB_PASS"), cfg.DB.Host, cfg.DB.Port, cfg.DB.Name)

	connectConfig, err := pgxpool.ParseConfig(conn)
	if err != nil {
		log.Error("Failed to parse connection string")
		return nil, fmt.Errorf("%s: %w", oper, err)
	}

	connectConfig.ConnConfig.ConnectTimeout = 5 * time.Second

	var db *pgxpool.Pool

	for i := 0; i < 10; i++ {
		db, err = pgxpool.NewWithConfig(ctx, connectConfig)
		if err != nil {
			log.Error(fmt.Sprintf("Failed connection - %d attempts left", 9-i))
			time.Sleep(time.Duration(i) * 100 * time.Millisecond)
		} else {
			log.Debug("Storage created")
			break
		}
	}

	if err != nil {
		log.Error("Storage was not created")
		return nil, fmt.Errorf("%s: %w", oper, err)
	}

	err = db.Ping(ctx)
	if err != nil {
		log.Error("failed Ping")
		return nil, fmt.Errorf("%s: %w", oper, err)
	}
	log.Debug("Ping is OK!")

	return &Storage{DB: db, log: log}, nil
}

func (s *Storage) Save(ctx context.Context, coins []entities.Coin) error {
	const op = "storage.postgres.Save"

	query := `
		insert into rates(name, currency, value, date_time)
		values(@Name, @Currency, @Value, @Time)
	`
	batch := &pgx.Batch{}

	for _, coin := range coins {
		args := pgx.NamedArgs{
			"Name":     coin.Name,
			"Currency": coin.Currency,
			"Value":    coin.Value,
			"Time":     coin.Time,
		}
		batch.Queue(query, args)
	}

	results := s.DB.SendBatch(ctx, batch)
	defer results.Close()

	for range coins {
		_, err := results.Exec()
		if err != nil {
			s.log.Error("saving Coins failed")
			return fmt.Errorf("%s: %w", op, err)
		}
	}
	s.log.Debug("Coins saved")
	return nil //*or results.Close()
}

func (s *Storage) MinDaily(ctx context.Context, name string) (entities.Coin, error) {
	const op = "storage.postgres.MinDaily"

	args := pgx.NamedArgs{
		"Name": name,
	}
	query := `
	select name, min(value) as value, currency, date_time
	from rates
	where DATE_TRUNC('day', date_time) = DATE_TRUNC('day', CURRENT_TIMESTAMP) and name = @Name
	group by name, currency, value, date_time
	limit 1;
	`

	rows, err := s.DB.Query(ctx, query, args)
	if err != nil {
		s.log.Error("failed query in MinDaily", err)
		return entities.Coin{}, fmt.Errorf("%s: %w", op, err)
	}
	defer rows.Close()

	coin := entities.Coin{}
	for rows.Next() {
		err = rows.Scan(&coin.Name, &coin.Value, &coin.Currency, &coin.Time)
		if err != nil {
			s.log.Error("failed scanning in MinDaily", err)
			return entities.Coin{}, fmt.Errorf("%s: %w", op, err)
		}
	}
	s.log.Debug("minimum value " + name + " received")
	return coin, nil
}

func (s *Storage) MaxDaily(ctx context.Context, name string) (entities.Coin, error) {
	const op = "storage.postgres.MaxDaily"

	args := pgx.NamedArgs{
		"Name": name,
	}
	query := `
	select name, max(value) as value, currency, date_time
	from rates
	where DATE_TRUNC('day', date_time) = DATE_TRUNC('day', CURRENT_TIMESTAMP) and name = @Name
	group by name, currency, value, date_time
	order by value desc
	limit 1;
	`
	rows, err := s.DB.Query(ctx, query, args)
	if err != nil {
		s.log.Error("failed query in MaxDaily", err)
		return entities.Coin{}, fmt.Errorf("%s: %w", op, err)
	}
	defer rows.Close()

	coin := entities.Coin{}
	for rows.Next() {
		err = rows.Scan(&coin.Name, &coin.Value, &coin.Currency, &coin.Time)
		if err != nil {
			s.log.Error("failed scanning in MaxDaily", err)
			return entities.Coin{}, fmt.Errorf("%s: %w", op, err)
		}
	}
	s.log.Debug("maximum value " + name + " received")
	return coin, nil
}

func (s *Storage) AvgDaily(ctx context.Context, name string) (entities.Coin, error) {
	const op = "storage.postgres.AvgDaily"

	args := pgx.NamedArgs{
		"Name": name,
	}
	query := `
	select ROUND(avg(value), 2) as value 
	from rates
	where DATE_TRUNC('day', date_time) = DATE_TRUNC('day', CURRENT_TIMESTAMP) and name = @Name;
	`
	rows, err := s.DB.Query(ctx, query, args)
	if err != nil {
		s.log.Error("failed query in AvgDaily", err)
		return entities.Coin{}, fmt.Errorf("%s: %w", op, err)
	}
	defer rows.Close()

	var value float64
	for rows.Next() {
		err = rows.Scan(&value)
		if err != nil {
			s.log.Error("failed scanning in AvgDaily", err)
			return entities.Coin{}, fmt.Errorf("%s: %w", op, err)
		}
	}

	coin := entities.Coin{
		Name:     name,
		Currency: "USD",
		Value:    value,
		Time:     time.Now().Truncate(24 * time.Hour),
	}
	s.log.Debug("average value " + name + " received")
	return coin, nil
}
