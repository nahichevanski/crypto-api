package client

import (
	"crypto-api/internal/entities"
	"crypto-api/pkg/cryptocompare"
	"encoding/json"
	"fmt"
	"time"
)

type CryptoClient struct {
}

func New() CryptoClient {
	return CryptoClient{}
}

func (c CryptoClient) GetRate(cryptoCoins ...string) ([]entities.Coin, error) {
	const op = "internal.adapters.client.cryptocompare.GetRate"

	//get rate from min-api.cryptocompare.com
	coinString, err := cryptocompare.RateFromCryptocompare(cryptoCoins...)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", op, err)
	}

	//decode into map
	var prices map[string]map[string]float64
	err = json.Unmarshal([]byte(coinString), &prices)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", op, err)
	}

	//create coin and append and return
	coins := make([]entities.Coin, 0, len(prices))
	for k, v := range prices {
		coin, err := entities.New(v[cryptocompare.USD], cryptocompare.USD, k, time.Now())
		if err != nil {
			return nil, fmt.Errorf("%s: %w", op, err)
		}
		coins = append(coins, coin)
	}

	return coins, nil
}
