package entities

import (
	"errors"
	"fmt"
	"time"
)

type Coin struct {
	Value    float64
	Currency string
	Name     string
	Time     time.Time
}

func New(val float64, cur, name string, t time.Time) (Coin, error) {

	if name != "BTC" && name != "ETH" {
		return Coin{}, errors.New("incorrect name of Coin, must be \"BTC\" or \"ETH\"")
	}

	coin := Coin{
		Value:    val,
		Currency: cur,
		Name:     name,
		Time:     t,
	}

	return coin, nil
}

func (c Coin) String() string {
	return fmt.Sprintf("%s %.2f$ %v", c.Name, c.Value, c.Time.Format(time.DateTime))
}
