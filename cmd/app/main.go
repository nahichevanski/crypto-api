package main

import (
	"context"
	"crypto-api/internal/adapters/client"
	"crypto-api/internal/adapters/storage/postgres"
	"crypto-api/internal/cases"
	"crypto-api/internal/config"
	"crypto-api/internal/ports/httpserver"
	"log/slog"
	"os"
	"time"
)

func main() {

	cfg := config.MustLoad()

	log := slog.New(
		slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}),
	)

	ctx := context.Background()

	db, err := postgres.New(ctx, log, cfg)
	if err != nil {
		log.Error("DataBase crashed", err)
		os.Exit(1)
	}
	log.Debug("DataBase connected")

	cc := client.New()
	s, err := cases.NewService(db, cc, log)
	if err != nil {
		log.Error("Service was not created")
		os.Exit(1)
	}
	log.Debug("Service created")

	srvr, err := httpserver.NewServer(s)
	if err != nil {
		log.Error("Server crashed", err)
		os.Exit(1)
	}
	log.Debug("Server created")

	go func() {
		for {
			coins, err := s.GetCrypto(cases.CryptoList)
			if err != nil {
				log.Error("failed cycle of async getting of coins", err)
			}
			err = s.SaveCrypto(ctx, coins)
			if err != nil {
				log.Error("failed cycle of async saving of coins to database", err)
			}
			time.Sleep(5 * time.Minute)
		}
	}()

	err = srvr.Run(cfg)
	if err != nil {
		log.Error("Server crashed", err)
		os.Exit(1)
	}
}
