package dto

import "time"

type Coin struct {
	Value    float64
	Currency string
	Name     string
	Time     time.Time
}
