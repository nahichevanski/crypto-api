CREATE TABLE IF NOT EXISTS rates (
	name VARCHAR(255),
	currency VARCHAR(255),
	value NUMERIC,
	date_time TIMESTAMP);
